package main

var x = [...]int{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 3, 4}

func Strip(str string) []byte {
	strLen := len(str)
	var (
		prv     byte
		i, j, l int
	)
	ret := make([]byte, strLen)
	for i < strLen {
		chr := str[i]
		i += x[(chr&0b11110000)>>4]
		switch chr {
		case '/':
			if prv == '/' {
				l += copy(ret[l:], str[j:i-2])
				prv = 0
				for i < strLen {
					chr = str[i]
					i += x[(chr&0b11110000)>>4]
					switch chr {
					case '\n':
						j = i - 1
						goto l1
					}
				}
				goto l2
			}
		case '*':
			if prv == '/' {
				l += copy(ret[l:], str[j:i-2])
				prv = 0
				for i < strLen {
					chr = str[i]
					i += x[(chr&0b11110000)>>4]
					switch chr {
					case '/':
						if prv == '*' {
							prv = 0
							j = i
							goto l1
						}
					}
					prv = chr
				}
				goto l2
			}
		case '"':
			if prv == '\\' {
				prv = 0
				goto l1
			}
			for i < strLen {
				chr = str[i]
				i += x[(chr&0b11110000)>>4]
				switch chr {
				case '"':
					if prv == '\\' {
						prv = 0
						continue
					}
					goto l1
				}
				prv = chr
			}
		}
		prv = chr
	l1:
	}
l2:
	if j < strLen {
		l += copy(ret[l:], str[j:])
	}
	return ret[:l]
}
func main() {
	str := `
	//this will be sripped
	/* this will be sripped */
	" //inside string won't be sripped "
	" /*inside string won't be sripped*/ "
	a
	//xxx
	"zxx//xxz"
	b
	/* yy/
	/yy */
	c
	"zx
	x\"x
	xz"
	a
	//xxx
	"zxx/*y*/xxz"
	b
	/* yy/*yy */
	c
	`
	//fmt.Printf("%x %x\n", str, "\r\n")
	println(string(Strip(str)))
}
